<div class="intro">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="section_title text-center">
            <h1>ADVANTURE & NATURE WAITING FOR YOU</h1> 
          </div>
        </div>
      </div>
      <div class="row intro_row">
        <div class="col-xl-8 col-lg-10 offset-xl-2 offset-lg-1">
          <div class="intro_text text-center">
            <p>Hiking is a fun, relaxing way to experience the great outdoors with friends and family. When you go on a hike, you get to explore places in nature that the roads cannot reach. Leave your worries about time, work and other stressors far behind and reawaken your sense of wonder. There is nothing like reconnecting with friends or loved ones on a journey through nature.s  </p>
          </div>
        </div>
      </div>
      <div class="row gallery_row">
        <div class="col">

          <!-- Gallery -->
          <div class="gallery_slider_container">
            <div class="owl-carousel owl-theme gallery_slider">
              
              <!-- Slide -->
              <div class="gallery_slide">
                <img src="images/menu5.jpg" alt="">
                <div class="gallery_overlay">
                  <div class="text-center d-flex flex-column align-items-center justify-content-center">
                    <a href="#">
                      <!-- <span>+</span>
                      <span>See More</span> -->
                    </a>
                  </div>
                </div>
              </div>

              <!-- Slide -->
              <div class="gallery_slide">
                <img src="images/menu1.jpg" alt="">
                <div class="gallery_overlay">
                  <div class="text-center d-flex flex-column align-items-center justify-content-center">
                    <a href="#">
                      <!-- <span>+</span>
                      <span>See More</span> -->	
                    </a>
                  </div>
                </div>
              </div>

              <!-- Slide -->
              <div class="gallery_slide">
                <img src="images/menu3.jpg" alt="">
                <div class="gallery_overlay">
                  <div class="text-center d-flex flex-column align-items-center justify-content-center">
                    <a href="#">
                      <!-- <span>+</span>
                      <span>See More</span> -->
                    </a>
                  </div>
                </div>
              </div>

              <!-- Slide -->
              <div class="gallery_slide">
                <img src="images/menu2.jpg" alt="">
                <div class="gallery_overlay">
                  <div class="text-center d-flex flex-column align-items-center justify-content-center">
                    <a href="#">
                      <!-- <span>+</span>
                      <span>See More</span> -->
                    </a>
                  </div>
                </div>
              </div>

            </div>
          </div>

        </div>
      </div>
    </div>
  </div>

  <!-- Rooms -->

  <div class="rooms_right container_wrapper">
    <div class="container">
      <div class="row row-eq-height">

        <!-- Rooms Image -->
        <div class="col-xl-6 order-xl-1 order-2">
          <div class="rooms_slider_container">
            <div class="owl-carousel owl-theme rooms_slider">
              
              <!-- Slide -->
              <div class="slide">
                <div class="background_image" style="background-image:url(images/tabur1.jpg)"></div>
              </div>

              <!-- Slide -->
              <div class="slide">
                <div class="background_image" style="background-image:url(images/tabur2.webp)"></div>
              </div>

              <!-- Slide -->
              <div class="slide">
                <div class="background_image" style="background-image:url(images/tabur3.jpg)"></div>
              </div>

            </div>
          </div>
        </div>

        <!-- Rooms Content -->
        <div class="col-xl-6 order-xl-2 order-1">
          <div class="rooms_right_content">
            <div class="section_title">
              <h1>Bukit Tabur, Selangor</h1>
            </div>
            <div class="rooms_text">
              <p>Bukit Tabur is a hill located in Taman Melawati , Malaysia . It is also known as Bukit Hangus. This hill is very prominent and can be seen from Jalan Lingkaran Tengah 2 Kuala Lumpur (MRR2).This hill, which is 396 meters (1,300 feet) high, is a quartz precipice. Bukit Tabur has a Bukit Dipterocarp forest . There is a lot of flora on this hill that has medicinal value.
Climbers enter via the Kampung Klang Gates trail, 50 meters from the entrance of the Klang Gates Dam, behind Taman Melawati for a quick 3-4 hour climb. Gombak Forest Reserve and Klang Gates Dam are located on the hillside. The peak provides an impressive view of the dam. The hill has several peaks that can be traversed. However, once through several peaks, the climber will head to a dead end. That's the last peak where climbers can see the whole scenery around it..</p>
            </div>
            <div class="rooms_list">
              <ul>
                <li class="d-flex flex-row align-items-center justify-content-start">
                  <img src="images/check.png" alt="">
                  <span>Dragon Backbone</span>
                </li>
                <li class="d-flex flex-row align-items-center justify-content-start">
                  <img src="images/check.png" alt="">
                  <span>Klang Gates Dam</span>
                </li>
                <li class="d-flex flex-row align-items-center justify-content-start">
                  <img src="images/check.png" alt="">
                  <span>Melawati hills</span>
                </li>
              </ul>
            </div>
            <div class="rooms_price">RM 40/<span>Night</span></div>
			<br>
           <div class="button discover_button"><a href="booking.php">See More</a></div> 
          </div>
        </div>

      </div>
    </div>
  </div>

  <!-- Rooms -->

  <div class="rooms_left container_wrapper">
    <div class="container">
      <div class="row row-eq-height">
        
        <!-- Rooms Content -->
        <div class="col-xl-6">
          <div class="rooms_left_content">
            <div class="section_title">
              <h1>Gunung Dato Negeri Sembilan</h1>
            </div>
            <div class="rooms_text">
              <p>Gunung Datok located in Rembau, Negeri Sembilan has a height of 884 meters (2,900.26 feet).Mount Datok has Hill Dipterocarp forest and Upper Dipterocarp forest . There is a lot of flora in this mountain that has medicinal value. It is a mountain that is often a destination for mountaineers. Along with Mount Tampin , Mount Datuk is one of the two mountains at the end of the Titiwangsa range</p>
            </div>
            <div class="rooms_list">
              <ul>
                <li class="d-flex flex-row align-items-center justify-content-start">
                  <img src="images/check.png" alt="">
                  <span>Boulder Climbing</span>
                </li>
                <li class="d-flex flex-row align-items-center justify-content-start">
                  <img src="images/check.png" alt="">
                  <span>Hang Tuah Footprint</span>
                </li>
                <li class="d-flex flex-row align-items-center justify-content-start">
                  <img src="images/check.png" alt="">
                  <span>Unobstructed views</span>
                </li>
              </ul>
            </div>
            <div class="rooms_price">RM 30/<span>Night</span></div>
			<br>
            <div class="button discover_button"><a href="booking.php">See More</a></div>
           
          </div>
        </div>

        <!-- Rooms Image -->
        <div class="col-xl-6">
          <div class="rooms_slider_container">
            <div class="owl-carousel owl-theme rooms_slider">
              
              <!-- Slide -->
              <div class="slide">
                <div class="background_image" style="background-image:url(images/datuk1.jpg)"></div>
              </div>

              <!-- Slide -->
              <div class="slide">
                <div class="background_image" style="background-image:url(images/datuk2.jpg)"></div>
              </div>

              <!-- Slide -->
              <div class="slide">
                <div class="background_image" style="background-image:url(images/datuk3.jpg)"></div>
              </div>
              
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  <!-- Discover -->

  <div class="discover">

    <!-- Discover Content -->
    <div class="discover_content">
      <div class="container">
        <div class="row">
          <div class="col-lg-5">
            <div class="section_title">
              <div></div>
              <h1>Discover HIKINGKE</h1>
            </div>
          </div>
        </div>
        <div class="row discover_row">
          <div class="col-lg-5">
            <div class="discover_highlight">
              <p>HIKINGKE, is a web-based system that is developed to solve the current problems that being face by alpinists regarding the complicated of the process to apply, cannot check the availability at the hiking places, book for hiking slot and guiders</p>
            
		
            <!-- <div class="button discover_button"><a href="#">discover</a></div> -->
          </div>
          <div class="col-lg-7">
            <div class="discover_text">
              <p>.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Discover Slider -->
    <div class="discover_slider_container">
      <div class="owl-carousel owl-theme discover_slider">
        
        <!-- Slide -->
		<body>
<!-- ijat--
<h1>This is a Heading</h1>
<p>This is a paragraph.</p>

</body>
        <div class="slide">
          <div class="background_image" style="background-image:url(images/discover_1.jpg)"></div>
          <div class="discover_overlay d-flex flex-column align-items-center justify-content-center">
            <h1><a href="#">Weddings</a></h1>
          </div>
        </div>

        <!-- Slide -->
        <div class="slide">
          <div class="background_image" style="background-image:url(images/discover_2.jpg)"></div>
          <div class="discover_overlay d-flex flex-column align-items-center justify-content-center">
            <h1><a href="#">Parties</a></h1>
          </div>
        </div>

        <!-- Slide -->
        <div class="slide">
          <div class="background_image" style="background-image:url(images/discover_3.jpg)"></div>
          <div class="discover_overlay d-flex flex-column align-items-center justify-content-center">
            <h1><a href="#">Relax</a></h1>
          </div>
        </div>

      </div>
    </div>

  </div>

  <!-- Testimonials -->

  <div class="testimonials">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="section_title text-center">
            <h1>Testimonials</h1>
          </div>
        </div>
      </div>
      <div class="row testimonials_row">
        <div class="col">
          
          <!-- Testimonials Slider -->
          <div class="testimonials_slider_container">
            <div class="owl-carousel owl-theme testimonials_slider">

              <!-- Slide -->
              <div>
                <div class="testimonial_text text-center">
                  <p>Nice interface and ease to use.</p>
                </div>
                <div class="testimonial_author text-center">
                  <div class="testimonial_author_image"><img src="images/testimonial.jpg" alt=""></div>
                  <div class="testimonial_author_name"><a href="#">Afiq,</a><span> Client</span></div>
                </div>
              </div>

              <!-- Slide -->
              <div>
                <div class="testimonial_text text-center">
                  <p>Just a perfect website.</p>
                </div>
                <div class="testimonial_author text-center">
                  <div class="testimonial_author_image"><img src="images/testimonial.jpg" alt=""></div>
                  <div class="testimonial_author_name"><a href="#">Zaid,</a><span> Client</span></div>
                </div>
              </div>

              <!-- Slide -->
              <div>
                <div class="testimonial_text text-center">
                  <p>Ease to use and user friendly.</p>
                </div>
                <div class="testimonial_author text-center">
                  <div class="testimonial_author_image"><img src="images/testimonial.jpg" alt=""></div>
                  <div class="testimonial_author_name"><a href="#">Ariffudin,</a><span> Client</span></div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
