<h1>About Us</h1>
  <div class="intro">
    <div class="container">
      <div class="row row-eq-height">

        <!-- Intro Content -->
        <div class="col-lg-6">
          <div class="intro_content">
            <div class="section_title">
              <h1>HIKINGKE</h1>
            </div>
            <div class="intro_text">
              <p>HIKINGKE is a web-based system that will help you to choose and book any interersting hiking places. HIKINGKE also developed to solve the current problems that being face by alpinists regarding the complicated of the process to apply, cannot check the availability at the hiking places, book for hiking slot and guiders. With this system, can give a lot of benefits and easiness to admin and users</p>
            </div>
           <!--  <div class="button intro_button"><a href="#">read more</a></div> -->
          </div>
        </div>

        <!-- Intro Image -->
        <div class="col-lg-6">
          <div class="intro_image">
            <div class="background_image" style="background-image:url(images/logoe.png)"></div>
            <img src="images/intro.jpg" alt="">
          </div>
        </div>

      </div>
    </div>
  </div>

  <!-- Offering -->

  <div class="offering">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="section_title text-center">
            <h1>What we offer</h1>
          </div>
        </div>
      </div>
      <div class="row offering_row">
        
        <!-- Offer Item -->
        <div class="col-xl-4 col-md-6">
          <div class="offer">
            <div class="offer_image"><img src="images/booking.jpg" alt=""></div>
            <div class="offer_content text-center">
              <div class="offer_title"><h3>Online Booking</h3></div>
              <div class="offer_text">
                <p>With HIKINGKE, you can book any interesting hiking places through online</p>
              </div>
            <!--   <div class="offer_button"><a href="#">discover</a></div> -->
            </div>
          </div>
        </div>

        <!-- Offer Item -->
        <div class="col-xl-4 col-md-6">
          <div class="offer">
            <div class="offer_image"><img src="images/cancel.png" alt=""></div>
            <div class="offer_content text-center">
			<br>
              <div class="offer_title"><h3>Cancel Booking</h3></div>
              <div class="offer_text">
                <p>With HIKINGKE, you can cancel any previous booking without any charge</p>
              </div>
            <!--   <div class="offer_button"><a href="#">discover</a></div> -->
            </div>
          </div>
        </div>

        <!-- Offer Item -->
        <div class="col-xl-4 col-md-6">
          <div class="offer">
            <div class="offer_image"><img src="images/fav.jpg" alt=""></div>
            <div class="offer_content text-center">
			<br>
			<br>
			<br>
              <div class="offer_title"><h3>Add to favourite</h3></div>
              <div class="offer_text">
                <p>You can choose any hiking places and add it to your favourite list</p>
              </div>
            <!--   <div class="offer_button"><a href="#">discover</a></div> -->
            </div>
          </div>
        </div>

        <!-- Offer Item -->
        <div class="col-xl-4 col-md-6">
          <div class="offer">
            <div class="offer_image"><img src="images/planning.png" alt=""></div>
            <div class="offer_content text-center">
              <div class="offer_title"><h3>Create Planner</h3></div>
              <div class="offer_text">
                <p>With HIKINGKE, you can create your own planner for your hiking activity</p>
              </div>
            <!--   <div class="offer_button"><a href="#">discover</a></div> -->
            </div>
          </div>
        </div>

        <!-- Offer Item -->
        <div class="col-xl-4 col-md-6">
          <div class="offer">
            <div class="offer_image"><img src="images/uiux.jpg" alt=""></div>
            <div class="offer_content text-center">
			<br>
			<br>
			<br>
              <div class="offer_title"><h3>Nice Interface</h3></div>
              <div class="offer_text">
                <p>HIKINGKE provide users with nice interface and will give them a good user experience</p>
              </div>
            <!--   <div class="offer_button"><a href="#">discover</a></div> -->
            </div>
          </div>
        </div>

        <!-- Offer Item -->
        <div class="col-xl-4 col-md-6">
          <div class="offer">
            <div class="offer_image"><img src="images/choice.jpg" alt=""></div>
            <div class="offer_content text-center">
              <div class="offer_title"><h3>Variety of choice</h3></div>
              <div class="offer_text">
                <p>HIKINGKE team will make sure that we will provide you with a variety of intereseting for you to choose</p>
              </div>
            <!--   <div class="offer_button"><a href="#">discover</a></div> -->
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  <!-- Discover -->

  <div class="discover">

    <!-- Discover Content -->
    <div class="discover_content">
      <div class="container">
        <div class="row">

          <!-- 
          <div class="col-xl-5 col-lg-6">
            <div class="section_title">
              <div>Hotel</div>
              <h1>Discover Marimar Hotel</h1>
            </div>
            <div class="discover_highlight">
              <p>Morbi tempus malesuada erat sed pellentesque. Donec pharetra mattis nulla, id laoreet neque scelerisque at. Quisque eget sem non ligula consectetur.</p>
            </div>
            <div class="discover_text">
              <p>Morbi tempus malesuada erat sed pellentesque. Donec pharetra mattis nulla, id laoreet neque scelerisque at. Quisque eget sem non ligula consectetur ultrices in quis augue. Donec imperd iet leo eget tortor dictum, eget varius eros sagittis. Curabitur tempor dignissim massa ut faucibus sollicitudin tinci dunt maximus. Morbi tempus malesuada erat sed pellentesque. Donec pharetra mattis.</p>
            </div>
            <div class="button discover_button"><a href="#">discover</a></div>
          </div>
-->
     
            </div>
          </div>

        </div>
      </div>
    </div>

  </div>
